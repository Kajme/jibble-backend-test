# Albums

## List Albums

### HTTP Request

`GET /api/albums`

> Create request

```bash
curl -X 
  GET /api/albums 
  -H 'authorization: --token--' 
  -H 'content-type: application/x-www-form-urlencoded'
```

```javascript
// jQuery
let settings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/albums",
  "method": "GET",
  "headers": {
    "authorization": "--token--",
  }
}
$.ajax(settings).done(function (response) {
  console.log(response);
});
```

### Response

Parameter | Type | Description
--------- | ---- | -----------
albums | Array | if `success:true` 
error | Object | if `success:false`

> Example response:

```json
[
    {
        "userId": 1,
        "id": 1,
        "title": "quidem molestiae enim"
    },
    {
        "userId": 1,
        "id": 2,
        "title": "sunt qui excepturi placeat culpa"
    },
    {
        "userId": 1,
        "id": 3,
        "title": "omnis laborum odio"
    }
]
```
