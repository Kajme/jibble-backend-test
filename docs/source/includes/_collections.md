# Collections

## All Collections

### HTTP Request

`GET /api/collections/all`

> Create request

```bash
curl -X 
  GET /api/collections 
  -H 'authorization: --token--' 
  -H 'content-type: application/x-www-form-urlencoded'
```

```javascript
// jQuery
let settings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/collections",
  "method": "GET",
  "headers": {
    "authorization": "--token--",
  }
}
$.ajax(settings).done(function (response) {
  console.log(response);
});
```

### Response

Parameter | Type | Description
--------- | ---- | -----------
post | Array | if `success:true` 
user | Array | if `success:true` 
album | Array | if `success:true` 
error | Object | if `success:false`

> Example response:

```json
[
    {
        "post": [
            {...}
        ],
        "album": [
            {
                "userId": 1,
                "id": 1,
                "title": "quidem molestiae enim"
            },
            {
                "userId": 1,
                "id": 2,
                "title": "sunt qui excepturi placeat culpa"
            },
            {
                "userId": 1,
                "id": 3,
                "title": "omnis laborum odio"
            }
        ],
        "user": [
            {...}
        ]
    }
]
```


## List Collections

### HTTP Request

`GET /api/collections`

> Create request

```bash
curl -X 
  GET /api/collections 
  -H 'authorization: --token--' 
  -H 'content-type: application/x-www-form-urlencoded'
```

```javascript
// jQuery
let settings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/collections",
  "method": "GET",
  "headers": {
    "authorization": "--token--",
  }
}
$.ajax(settings).done(function (response) {
  console.log(response);
});
```

### Response

Parameter | Type | Description
--------- | ---- | -----------
post | Array | if `success:true` 
user | Array | if `success:true` 
album | Array | if `success:true` 
error | Object | if `success:false`

> Example response:

```json
[
    {
        "user": {
            "id": 1,
            "name": "Leanne Graham",
            "username": "Bret",
            "email": "Sincere@april.biz",
            "address": {
                "street": "Kulas Light",
                "suite": "Apt. 556",
                "city": "Gwenborough",
                "zipcode": "92998-3874",
                "geo": {
                    "lat": "-37.3159",
                    "lng": "81.1496"
                }
            },
            "phone": "1-770-736-8031 x56442",
            "website": "hildegard.org",
            "company": {
                "name": "Romaguera-Crona",
                "catchPhrase": "Multi-layered client-server neural-net",
                "bs": "harness real-time e-markets"
            }
        },
        "post": {
            "userId": 1,
            "id": 1,
            "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
            "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
        },
        "album": {
            "userId": 1,
            "id": 1,
            "title": "quidem molestiae enim"
        }
    },
    {
        "user": {
            "id": 2,
            "name": "Ervin Howell",
            "username": "Antonette",
            "email": "Shanna@melissa.tv",
            "address": {
                "street": "Victor Plains",
                "suite": "Suite 879",
                "city": "Wisokyburgh",
                "zipcode": "90566-7771",
                "geo": {
                    "lat": "-43.9509",
                    "lng": "-34.4618"
                }
            },
            "phone": "010-692-6593 x09125",
            "website": "anastasia.net",
            "company": {
                "name": "Deckow-Crist",
                "catchPhrase": "Proactive didactic contingency",
                "bs": "synergize scalable supply-chains"
            }
        },
        "post": {
            "userId": 1,
            "id": 2,
            "title": "qui est esse",
            "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
        },
        "album": {
            "userId": 1,
            "id": 2,
            "title": "sunt qui excepturi placeat culpa"
        }
    },
]
```
