# Posts

## List Posts

### HTTP Request

`GET /api/posts`

> Create request

```bash
curl -X 
  GET /api/posts 
  -H 'authorization: --token--' 
  -H 'content-type: application/x-www-form-urlencoded'
```

```javascript
// jQuery
let settings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/posts",
  "method": "GET",
  "headers": {
    "authorization": "--token--",
  }
}
$.ajax(settings).done(function (response) {
  console.log(response);
});
```

### Response

Parameter | Type | Description
--------- | ---- | -----------
posts | Array | if `success:true` 
error | Object | if `success:false`

> Example response:

```json
[
    {
        "userId": 1,
        "id": 1,
        "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    },
    {
        "userId": 1,
        "id": 2,
        "title": "qui est esse",
        "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    }
]
```




## Create Post

### HTTP Request

`POST /api/posts`

> Create request

```bash
curl -X 
  POST /api/posts 
  -H 'authorization: --token--' 
  -H 'content-type: application/x-www-form-urlencoded'
  -d 'title=example&body=example&userid=1'
```

```javascript
// jQuery
let settings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/posts",
  "method": "POST",
  "headers": {
    "authorization": "--token--",
  },
  "data": {
    "title": "example",
    "body": "example",
    "userId": 1
  }
}
$.ajax(settings).done(function (response) {
  console.log(response);
});
```

### Response

Parameter | Type | Description
--------- | ---- | -----------
posts | Object | if `success:true` 
error | Object | if `success:false`

> Example response:

```json
[{"id": 101}]
```




## Edit Post

While editing post, you need to send the whole payload.

### HTTP Request

`PUT /api/posts/:postId`

> Create request

```bash
curl -X 
  PUT /api/posts/1 
  -H 'authorization: --token--' 
  -H 'content-type: application/x-www-form-urlencoded'
  -d 'title=example&body=example&userid=1'
```

```javascript
// jQuery
let settings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/posts/1",
  "method": "PUT",
  "headers": {
    "authorization": "--token--",
  },
  "data": {
    "title": "example",
    "body": "example",
    "userId": 1
  }
}
$.ajax(settings).done(function (response) {
  console.log(response);
});
```

### Response

Parameter | Type | Description
--------- | ---- | -----------
posts | Object | if `success:true` 
error | Object | if `success:false`

> Example response:

```json
[{"id": 101}]
```





## Change Post

While changing post, you dont need to send the whole payload, but just the field you want to change.

### HTTP Request

`PATCH /api/posts/:postId`

> Create request

```bash
curl -X 
  PATCH /api/posts/1 
  -H 'authorization: --token--' 
  -H 'content-type: application/x-www-form-urlencoded'
  -d 'title=example'
```

```javascript
// jQuery
let settings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/posts/1",
  "method": "PATCH",
  "headers": {
    "authorization": "--token--",
  },
  "data": {
    "title": "example",
  }
}
$.ajax(settings).done(function (response) {
  console.log(response);
});
```

### Response

Parameter | Type | Description
--------- | ---- | -----------
posts | Object | if `success:true` 
error | Object | if `success:false`

> Example response:

```json
[{"id": 101}]
```




## Delete Post

While changing post, you dont need to send the whole payload, but just the field you want to change.

### HTTP Request

`DELETE /api/posts/:postId`

> Create request

```bash
curl -X 
  DELETE /api/posts/1 
  -H 'authorization: --token--' 
  -H 'content-type: application/x-www-form-urlencoded'
```

```javascript
// jQuery
let settings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/posts/1",
  "method": "DELETE",
  "headers": {
    "authorization": "--token--",
  }
}
$.ajax(settings).done(function (response) {
  console.log(response);
});
```

### Response

Parameter | Type | 
--------- | -------| ------------------------
posts | Object | if `success:true` 
error | Object | if `success:false`

> Example response:

```json
[{"id": 101}]
```