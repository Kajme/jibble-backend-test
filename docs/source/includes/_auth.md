# Authentication

Authenticate your account when using the API by providing hitting the `/api/auth/token` route.

API expects for the token to be included in all API requests to the server in a header that looks like the following:

`Authorization: --token--`

<aside class="notice">
You must replace <code>--token--</code> with token that you receive from API when you authenticate.
</aside>

### HTTP Request

`GET /api/auth/token`

> Create request

```bash
curl -X 
  GET /api/auth/token 
  -H 'authorization: --token--' 
  -H 'content-type: application/x-www-form-urlencoded'
```

```javascript
// jQuery
let settings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/auth/token",
  "method": "GET",
  "headers": {
    "authorization": "--token--",
  }
}
$.ajax(settings).done(function (response) {
  console.log(response);
});
```

### Response

Parameter | Type | Description
--------- | ---- | -----------
success | Bool | `true` or `false`
token | String | if `success:true` 

> Example response:

```json
{ 
  "success": true, 
  "token": "--token--", 
}
```
