# Users

## List Users

### HTTP Request

`GET /api/users`

> Create request

```bash
curl -X 
  GET /api/users 
  -H 'authorization: --token--' 
  -H 'content-type: application/x-www-form-urlencoded'
```

```javascript
// jQuery
let settings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/users",
  "method": "GET",
  "headers": {
    "authorization": "--token--",
  }
}
$.ajax(settings).done(function (response) {
  console.log(response);
});
```

### Response

Parameter | Type | 
--------- | -------| ------------------------
users | Array | if `success:true` 
error | Object | if `success:false`

> Example response:

```json
[
    {
        "id": 1,
        "name": "Leanne Graham",
        "username": "Bret",
        "email": "Sincere@april.biz",
        "address": {
            "street": "Kulas Light",
            "suite": "Apt. 556",
            "city": "Gwenborough",
            "zipcode": "92998-3874",
            "geo": {
                "lat": "-37.3159",
                "lng": "81.1496"
            }
        },
        "phone": "1-770-736-8031 x56442",
        "website": "hildegard.org",
        "company": {
            "name": "Romaguera-Crona",
            "catchPhrase": "Multi-layered client-server neural-net",
            "bs": "harness real-time e-markets"
        }
    },
    {
        "id": 2,
        "name": "Ervin Howell",
        "username": "Antonette",
        "email": "Shanna@melissa.tv",
        "address": {
            "street": "Victor Plains",
            "suite": "Suite 879",
            "city": "Wisokyburgh",
            "zipcode": "90566-7771",
            "geo": {
                "lat": "-43.9509",
                "lng": "-34.4618"
            }
        },
        "phone": "010-692-6593 x09125",
        "website": "anastasia.net",
        "company": {
            "name": "Deckow-Crist",
            "catchPhrase": "Proactive didactic contingency",
            "bs": "synergize scalable supply-chains"
        }
    }
]
```
