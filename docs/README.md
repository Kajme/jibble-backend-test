# Getting Started
- docs uses slate.io as base

## Install 
  `npm install`

## Run 
  `npm start`

## Export to ../static/docs/
  `npm run generate`


## todo
- add auto generate for deploy

## Whiteboard
#### Simply write beautiful API documentation.
========

<cite>This project started as a fork of the popular [Slate](https://github.com/tripit/slate) API documentation tool, which uses ruby. Since I found the initial setup of Slate quite cumbersome, I started this NodeJS based project.</cite>

Check out a Whiteboard [example API documentation](http://marcelpociot.de/whiteboard/).


