
process.env = require('../../config/env/test');
const server = process.env.server;

const chai = require('chai');
const chaiHttp = require('chai-http');
const chaiJWT = require('chai-jwt');
const should = chai.should();
const jwt = require('jsonwebtoken');

chai.use(chaiHttp);
chai.use(chaiJWT);

let returnedToken;
let decodedToken;

describe('Posts', () => {

    /**
     * Test GET /api/posts without token, take token and then try again
     */
    describe('list', () => {

        it('should return 501 without token', (done) => {
            chai.request(server)
            .get('/api/posts')
            .end((err, res) => {
                res.should.have.status(501);
                done();
            });
        });

        it('should fetch the token', (done) => {
            // Get a token and then test
            chai.request(server)
            .get('/api/auth/token')
            .end((err, res) => {
                returnedToken = JSON.parse(res.text).token;
                decodedToken = jwt.verify(returnedToken, process.env.jwtSecret);
                done();
            });
        });
    
        it('should return 200 with token', (done) => {
            chai.request(server)
            .get('/api/posts')
            .set('authorization', returnedToken)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

    });

    /**
     * Test POST /api/posts without token, take token and then try again
     */
    describe('create', () => {
        
        it('should return 501 without token', (done) => {
            chai.request(server)
            .get('/api/posts')
            .end((err, res) => {
                res.should.have.status(501);
                done();
            });
        });
    
        it('should create new post', (done) => {
            chai.request(server)
            .post('/api/posts')
            .send({
                userId: 1,
                title: 'test title',
                body: 'test body'
            })
            .set('authorization', returnedToken)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

    });


    /**
     * Test PUT /api/posts without token, take token and then try again
     */
    describe('edit', () => {
        
        it('should return 501 without token', (done) => {
            chai.request(server)
            .put('/api/posts')
            .end((err, res) => {
                res.should.have.status(501);
                done();
            });
        });
    
        it('should update the post', (done) => {
            chai.request(server)
            .put('/api/posts/1')
            .set({
                'authorization': returnedToken,
                'Content-type': 'application/json; charset=UTF-8'
            })
            .send({
                id: 1,
                title: 'test title',
                body: 'lorem ipsum dolor sit amet...',
                userId: 1
            })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

    });

    /**
     * Test PUT /api/posts without token, take token and then try again
     */
    describe('edit partial', () => {
        
        it('should return 501 without token', (done) => {
            chai.request(server)
            .patch('/api/posts')
            .end((err, res) => {
                res.should.have.status(501);
                done();
            });
        });
    
        it('should update the partial of post', (done) => {
            chai.request(server)
            .patch('/api/posts/1')
            .set({
                'authorization': returnedToken,
                'Content-type': 'application/json; charset=UTF-8'
            })
            .send({
                body: 'lorem ipsum dolor sit amet...'
            })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

    });

    /**
     * Test GET /api/posts without token, take token and then try again
     */
    describe('delete', () => {
        
        it('should return 501 without token', (done) => {
            chai.request(server)
            .get('/api/posts')
            .end((err, res) => {
                res.should.have.status(501);
                done();
            });
        });
    
        it('should delete the post', (done) => {
            chai.request(server)
            .delete('/api/posts/1')
            .set('authorization', returnedToken)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

    });

});