
process.env = require('../../config/env/test');
const server = process.env.server;

const chai = require('chai');
const chaiHttp = require('chai-http');
const chaiJWT = require('chai-jwt');
const should = chai.should();
const jwt = require('jsonwebtoken');

chai.use(chaiHttp);
chai.use(chaiJWT);

describe('Albums', () => {

    /**
     * Test GET /api/albums without token, take token and then try again
     */
    describe('list', () => {

        it('should return 501 without token', (done) => {
            chai.request(server)
            .get('/api/albums')
            .end((err, res) => {
                res.should.have.status(501);
                done();
            });
        });

        let returnedToken;
        let decodedToken;
        it('should fetch the token', (done) => {
            // Get a token and then test
            chai.request(server)
            .get('/api/auth/token')
            .end((err, res) => {
                returnedToken = JSON.parse(res.text).token;
                decodedToken = jwt.verify(returnedToken, process.env.jwtSecret);
                done();
            });
        });
    
        it('should return 200 with token', (done) => {
            chai.request(server)
            .get('/api/albums')
            .set('authorization', returnedToken)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

    });

});