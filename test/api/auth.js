
process.env = require('../../config/env/test');
const server = process.env.server;

const chai = require('chai');
const chaiHttp = require('chai-http');
const chaiJWT = require('chai-jwt');
const should = chai.should();
const jwt = require('jsonwebtoken');

chai.use(chaiHttp);
chai.use(chaiJWT);

describe('Auth', () => {

    /**
     * Test GET /api/auth/token
     */
    describe('get', () => {

        let returnedToken;
        let decodedToken;
        it('should fetch the token', (done) => {
            // Get a token and then test
            chai.request(server)
            .get('/api/auth/token')
            .end((err, res) => {
                returnedToken = JSON.parse(res.text).token;
                decodedToken = jwt.verify(returnedToken, process.env.jwtSecret);
                done();
            });
        });

    });

});