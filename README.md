# Jibble Backend Test

Backend Test Project - Boban Karisik


## Development


### Prerequisites

 - [Docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/)
 - [Node.js and npm](https://nodejs.org/)


### Running

Install the dependencies

    $ npm install

Configure the environment variables

    $ cp .env.example .env
    $ edit .env

Start the infrastructure

    $ bin/infra/start

__notice__ Starting infrastructure is required only if you are using route cache

Start the app in `development` mode

    $ bin/server/dev

Start the app in `production` mode

    $ bin/server/prod

Open browser and visit `http://localhost:3000/`

After you're done, stop the infrastructure

    $ bin/infra/stop


### Testing

Edit `config/env/test.js`

    $ bin/server/dev
    $ bin/test

## Docs
After the server is running, visit `/docs`

For more info 
- refer to docs/README.md


## Authors

* **[Boban Karisik](https://karisik.online)** - *Initial work*

