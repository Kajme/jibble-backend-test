'use strict';

/**
 * Expose
 */

module.exports = {
  server: 'http://localhost:3000',
  jwtSecret: 'super awesome secret'
};