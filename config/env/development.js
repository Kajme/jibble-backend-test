'use strict';

/**
 * Expose
 */

module.exports = {
  db: 'mongodb',
  redis: 'redis',
  jwtSecret: 'super awesome secret'
};