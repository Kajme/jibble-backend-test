'use strict';

/**
 * Expose
 */
module.exports = {
  db: process.env.MONGO,
  redis: process.env.REDIS,
  jwtSecret: process.env.JWT
};