'use strict';

/*
 * Module dependencies.
 */

const express = require('express');
const users = require('../app/controllers/users');
const posts = require('../app/controllers/posts');
const albums = require('../app/controllers/albums');
const collections = require('../app/controllers/collections');
const auth = require('./middlewares/auth');

/**
 * Route middlewares
 */

// const cache = require('./middlewares/cache');
// const throttle = require('./middlewares/throttle');
// const cacheThrottle = [cache, throttle];


/**
 * Expose routes
 */

module.exports = function (app) {
  

  // api routes
  const api = express.Router();
    // get auth token
    api.get('/auth/token', users.token);
    // user routes
    api.get('/users', auth.jwt, users.list);
    // albums routes
    api.get('/albums', auth.jwt, albums.list);
    // posts routes
    api.get('/posts', auth.jwt, posts.list);
    api.post('/posts', auth.jwt, posts.create);
    api.put('/posts/:postId', auth.jwt, posts.update);
    api.patch('/posts/:postId', auth.jwt, posts.patch);
    api.delete('/posts/:postId', auth.jwt, posts.delete);
    // collections routes
    api.get('/collections', auth.jwt, collections.list);
    api.get('/collections/all', auth.jwt, collections.all);

  // api routes
  app.use('/api', api);

  // home route
  app.all('/', function (req, res) {
    return res.json({ success: true, name: 'Jibble backend test api', version: '0.0.1' });
  });



  /**
   * Error handling
   */

  app.use(function (err, req, res, next) {
    // treat as 501
    if (err.message
      && (~err.message.indexOf('not found')
      || (~err.message.indexOf('Cast to ObjectId failed')))) {
      return next();
    }

    if (err.stack.includes('ValidationError')) {
      res.status(501).json({ error: err.stack });
      return;
    }

    // error page
    return res.status(501).json({ error: err.stack });
  });

  // assume 501 since no middleware responded
  app.use(function (req, res) {
    const payload = {
      url: req.originalUrl,
      error: 'Not found'
    };
    return res.status(501).json(payload);
  });
};