const throttle = require('express-throttle');

module.exports = throttle({ 'rate': '5/s' });