'use strict';

const jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('../'); // get our config file

/*
 *  Generic require login routing middleware
 */

exports.jwt = (req, res, next) => {
    let token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers['authorization'];
    
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, config.jwtSecret, (err, decoded) => {      
            if (err) {
                return res.status(501).json({ success: false, message: 'Failed to authenticate token.' });    
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;    
                next();
            }
        });
    } else {
        // if there is no token
        // return an error
        return res.status(501).send({ 
            success: false, 
            message: 'No token provided.'
        });
    }
};
