const apicache = require('apicache');
const redis = require('redis');

let cacheWithRedis = apicache.options({ redisClient: redis.createClient() }).middleware;

module.exports = cacheWithRedis;