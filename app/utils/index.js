'use strict';

/**
 * Module dependencies.
 */
const request = require('request-promise');

module.exports = {
    respond: (status, obj, res) => {
        res.format({
            html: () => {
                if (status) return res.status(status).json(obj);
                return res.json(obj);
            },
            json: () => {
                if (status) return res.status(status).json(obj);
                return res.json(obj);
            }
        });
    },
    request: async (options) => {
        return await request(options);
    }
};