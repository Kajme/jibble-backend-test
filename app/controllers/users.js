'use strict';

/**
 * Module dependencies.
 */

const config = require('../../config');
const jwt = require('jsonwebtoken');
const { request, respond } = require('../utils');

const generateToken = (user) => {
    return jwt.sign({
        id: user
    }, config.jwtSecret, {
        expiresIn: 3600 // 1 hour
    });
};

/**
 * Login/Signin
 */
exports.token = (req, res) => {
    let token = generateToken(new Date());
    return respond(200, { success: true, token: token }, res);
};

/**
 * List
 */
exports.list = async (req, res) => {
    let options = {
        uri: 'http://jsonplaceholder.typicode.com/users',
        json: true // Automatically parses the JSON string in the response
    };
    try {
        let users = await request(options);
        return respond(200, users.slice(0, 30), res);
    } catch (e) {
        return respond(501, e, res);
    }
};
