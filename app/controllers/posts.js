'use strict';

/**
 * Module dependencies.
 */
const { request, respond } = require('../utils');
const baseUrl = 'http://jsonplaceholder.typicode.com/posts';

/**
 * Create
 */
exports.create = async (req, res) => {
    let options = {
        method: 'POST',
        uri: baseUrl,
        body: JSON.stringify(req.body),
    };
    try {
        let post = await request(options);
        respond(200, post, res);
    } catch (e) {
        respond(501, e, res);
    }
};


/**
 * List
 */
exports.list = async (req, res) => {
    let options = {
        uri: baseUrl,
        json: true // Automatically parses the JSON string in the response
    };
    try {
        let posts = await request(options);
        return respond(200, posts.slice(0, 30), res);
    } catch (e) {
        return respond(501, e, res);
    }
};

/**
 * Update
 */
exports.update = async (req, res) => {
    let options = {
        method: 'PUT',
        uri: `${baseUrl}/${req.params.postId}`,
        body: JSON.stringify(req.body),
    };
    try {
        let post = await request(options);
        respond(200, post, res);
    } catch (e) {
        respond(501, e, res);
    }
};

/**
 * Patch
 */
exports.patch = async (req, res) => {
    let options = {
        method: 'PATCH',
        uri: `${baseUrl}/${req.params.postId}`,
        body: JSON.stringify(req.body),
    };
    try {
        let post = await request(options);
        respond(200, post, res);
    } catch (e) {
        respond(501, e, res);
    }
};


/**
 * Delete
 */
exports.delete = async (req, res) => {
    let options = {
        method: 'DELETE',
        uri: `${baseUrl}/${req.params.postId}`,
    };
    try {
        let post = await request(options);
        respond(200, post, res);
    } catch (e) {
        respond(501, e, res);
    }
};