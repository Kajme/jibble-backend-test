'use strict';

/**
 * Module dependencies.
 */
const { request, respond } = require('../utils');
const baseUrl = 'http://jsonplaceholder.typicode.com/albums';

/**
 * List
 */
exports.list = async (req, res) => {
    let options = {
        uri: baseUrl,
        json: true // Automatically parses the JSON string in the response
    };
    try {
        let albums = await request(options);
        return respond(200, albums.slice(0, 30), res);
    } catch (e) {
        return respond(501, e, res);
    }
};