'use strict';

/**
 * Module dependencies.
 */
const { request, respond } = require('../utils');
const baseUrl = 'http://jsonplaceholder.typicode.com';

/**
 * All
 */
exports.all = async (req, res) => {
    // get albums
    let options = {
        uri: `${baseUrl}/albums`,
        json: true // Automatically parses the JSON string in the response
    };
    let albums = await request(options);

    // get posts
    options.uri = `${baseUrl}/posts`;
    let posts = await request(options);

    // get users
    options.uri = `${baseUrl}/users`;
    let users = await request(options);
    
    return respond(200, { post: posts.slice(0, 30), album: albums.slice(0, 30), user: users.slice(0, 30) }, res);
};

/**
 * List
 */
exports.list = async (req, res) => {
    // get albums
    let options = {
        uri: `${baseUrl}/albums`,
        json: true // Automatically parses the JSON string in the response
    };
    let albums = (await request(options)).slice(0, 30);

    // get posts
    options.uri = `${baseUrl}/posts`;
    let posts = (await request(options)).slice(0, 30);

    // get users
    options.uri = `${baseUrl}/users`;
    let users = (await request(options)).slice(0, 30);

    let collections = users.reduce((collections, user, index) => {
        collections.push({
            user,
            post: posts[index],
            album: albums[index]
        });
        return collections;
    }, []);
    
    return respond(200, collections, res);
};